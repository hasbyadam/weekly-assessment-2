
# OH WEEKLY ASSESSMENT 2

In this assessment, you have to answer some basic question about Internet. You can find the answer from the internet or by discussing with your friend, but make sure you understand what you are doing. Please deliver this assessment on time no matter what results you make! Thank you.

Deadline: Saturday, 28 December 2021, 23:59 WIB

## Instructions
- Clone this project repository
- Try to complete complete the task
- Create git repository using `weekly-assessment-2` as the name of project
- Push your result to it
- Fill this form when you are finish: https://docs.google.com/forms/d/e/1FAIpQLSfe-kDIu-efXh_62tq3dAl3DwuN9PB2Pae0ywYPKSMUiM0sOw/viewform

## Task Requirements
- [ ] Complete: Answer all Question
  
Question:

1. Explain How the Internet Works.

2. What's the meaning of communication protocol

3. Mention HTTP status codes

4. Make Analogy Regarding how Front end and Backend communicate each other

5. Mention What method in HTTP protocol

Answer:

1. Internet bekerja dengan menghubungkan komputer sehingga dapat mengirim dan menerima data menggunakan protokol sehingga menciptakan jaringan    interaksi yang dapat diakses dan dipahami oleh user.  

 2. Protokol komunikasi adalah sistem aturan yang memungkinkan dua atau lebih entitas dari sistem komunikasi  untuk    mengirimkan informasi melalui segala jenis variasi kuantitas fisik. Protokol mendefinisikan aturan, sintaks, semantik dan sinkronisasi komunikasi dan kemungkinan metode pemulihan kesalahan. Protokol dapat diimplementasikan oleh perangkat keras, perangkat lunak, atau kombinasi keduanya.

3. Status code dikeluarkan oleh server sebagai tanggapan atas permintaan client yang dibuat ke server.Semua     kode status respons HTTP dipisahkan menjadi lima kelas atau kategori. Digit pertama kode status mendefinisikan kelas respons, sedangkan dua digit terakhir tidak memiliki peran klasifikasi atau kategorisasi. Ada lima kelas yang ditentukan oleh standar:
  *1xx tanggapan informasional – permintaan telah diterima, proses berlanjut
  *2xx berhasil – permintaan berhasil diterima, dipahami, dan diterima
  *Pengalihan 3xx – tindakan lebih lanjut perlu diambil untuk menyelesaikan permintaan
  *Kesalahan klien 4xx – permintaan berisi sintaks yang buruk atau tidak dapat dipenuhi
  *Kesalahan server 5xx – server gagal memenuhi permintaan yang tampaknya valid

4. Jika terdapat suatu restoran, maka front end adalah proses pemilihan menu yang dilakukan oleh pelanggan yang kemudian
   disampaikan melalui waiter yaitu API yang diteruskan ke dapur yang dapat disamakan sebagai backend untuk diproses dan
   dikembalikan ke waiter untuk diterima oleh pelanggan.

5.  Get     : Untuk request data dari resource tertentu
    Post    : Untuk Mengirim data ke server untuk memperbarui atau membuat resource
    Put     : sama dengan post, namun jika dilakukan berulang akan selalu menghasilkan hasil yg sama
    Head    : Mirip dengan Get , namun tanpa mendapatkan datanya atau hanya untuk mengecek
    Delete  : Menghapus resource tertentu
    Option  : Menjelaskan opsi komunikasi untuk resource target


  ## Please answer this question in markdown format